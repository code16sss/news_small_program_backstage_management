<?php
/**
 *
 * User: 16
 * Date: 2018/3/20
 * Time: 10:36
 */
/** 登陆 */
Route::group('admin',[
    'login'   => ['admin/Auth/login', ['method' => 'get']],
    'new'   => ['admin/New/index', ['method' => 'get']],
    'addNew'   => ['admin/New/add', ['method' => 'get']],
    'getNews'   => ['admin/New/getNews', ['method' => 'get']],
    'editNew/:id'   => ['admin/New/edit', ['method' => 'get']],

    'payContent'   => ['admin/Pay/index', ['method' => 'get']],
    'addPay'   => ['admin/Pay/add', ['method' => 'get']],
    'getPay'   => ['admin/Pay/getPay', ['method' => 'get']],
    'editPay/:id'   => ['admin/Pay/edit', ['method' => 'get']],

    'user'   => ['admin/User/index', ['method' => 'get']],
    'getUser'   => ['admin/User/getUser', ['method' => 'get']],

    'broke'   => ['admin/Broke/index', ['method' => 'get']],
    'getBroke'   => ['admin/Broke/getBroke', ['method' => 'get']],
    'seeDetail/:id'   => ['admin/Broke/detail', ['method' => 'get']],

    'complaint'   => ['admin/Complaint/index', ['method' => 'get']],
    'getComplaint'   => ['admin/Complaint/getComplaint', ['method' => 'get']],

    'getOrder'   => ['admin/Order/getOrder', ['method' => 'get']],
    'order/[:pid]/[:uid]'   => ['admin/Order/index', ['method' => 'get']],

    'clearcache'   => ['admin/Cache/clear', ['method' => 'get']],  //清除缓存


]);