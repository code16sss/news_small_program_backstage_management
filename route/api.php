<?php
/**
 *
 * User: 16
 * Date: 2018/3/14
 * Time: 9:24
 */

Route::resource('api/:version/news','api/:version.News');                   //新闻
Route::rule('api/:version/getSwiper','api/:version.News/getTopLists');    //轮播
Route::rule('api/:version/newtype','api/:version.News/getType');    //新闻分类

Route::resource('api/:version/pay','api/:version.Pay');                     //付费
Route::rule('api/:version/payUser/:id','api/:version.Pay/getPayUser');    //付费内容用户
Route::resource('api/:version/user','api/:version.User');                   //用户

Route::rule('api/:version/upload','api/:version.Upload/uploadImg');    //上传图片接口
Route::resource('api/:version/broke','api/:version.Broke');                   //用户

Route::resource('api/:version/complaint','api/:version.Complaint');                   //投诉

Route::rule('api/:version/login','api/:version.User/login','POST')->only(['psw','account']);   //登陆
Route::rule('api/:version/exit','api/:version.User/loginExit');   //推出登陆

Route::rule('api/token','api/Auth/token');    //轮播
Route::rule('api/:version/sendCode','api/:version.User/sendCode');    //获取openid
Route::rule('api/:version/updateAdminPsw','api/:version.User/updateAdminPsw');    //获取openid

Route::rule('api/:version/payCallback','api/:version.Order/payCallback');    //支付成功回调





