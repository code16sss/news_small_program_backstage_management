<?php
/**
 * 标签添加验证类
 * User: 16
 * Date: 2018/1/25
 */
namespace app\api\validate;

use think\Validate;
use data\model\Article as ArticleModel;

class complaint extends Validate
{
    protected $rule =   [
        'username'  => 'require',
        'user'  => 'require',
        'address'  => 'require',
        'content'   => 'require',
        'mobile'   => 'require',
    ];

    protected $message  =   [
        'username.require'     => '姓名不能为空',
        'user.require'     => '用户不能为空',
        'address.require'     => '投递地址不能为空',
        'content.require'      => '投诉详情不能为空',
        'mobile.require'      => '手机号码不能为空',
    ];

}