<?php
/**
 * 标签添加验证类
 * User: 16
 * Date: 2018/1/25
 */
namespace app\api\validate;

use think\Validate;
use data\model\Article as ArticleModel;

class pay extends Validate
{
    protected $rule =   [
        'title'  => 'require',
        'cover'  => 'require',
        'content'   => 'require',
    ];

    protected $message  =   [
        'title.require'     => '标题不能为空',
        'cover.require'     => '请选择封面',
        'content.require'      => '请输入内容',
    ];

}