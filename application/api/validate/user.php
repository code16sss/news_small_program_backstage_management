<?php
/**
 * 标签添加验证类
 * User: 16
 * Date: 2018/1/25
 */
namespace app\api\validate;

use think\Validate;
use data\model\Article as ArticleModel;

class user extends Validate
{
    protected $rule =   [
        'openid'  => 'require',
        'nickname'  => 'require',
        'avatarurl'  => 'require',
        'account'  => 'require',
        'psw'  => 'require',
    ];

    protected $message  =   [
        'openid.require'     => 'openid不能为空',
        'nickname.require'     => '用户名不能为空',
        'avatarurl.require'      => '头像不能为空',
        'account.require'      => '账号不能为空',
        'psw.require'      => '密码不能为空',
    ];

    protected $scene = [
        'add'  =>  ['openid','nickname','avatarurl'],
        'validate'  =>  ['username','psw'],
    ];

}