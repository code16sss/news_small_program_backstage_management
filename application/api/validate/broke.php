<?php
/**
 * 标签添加验证类
 * User: 16
 * Date: 2018/1/25
 */
namespace app\api\validate;

use think\Validate;
use data\model\Article as ArticleModel;

class broke extends Validate
{
    protected $rule =   [
        'contact'  => 'require',
        'desc'  => 'require',
        'content'   => 'require',
        'user'   => 'require',
    ];

    protected $message  =   [
        'contact.require'     => '联系方式不能为空',
        'desc.require'     => '说明不能为空',
        'content.require'      => '请输入内容',
        'user.require'      => '用户不能为空',
    ];

}