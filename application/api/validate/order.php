<?php
/**
 * 标签添加验证类
 * User: 16
 * Date: 2018/1/25
 */
namespace app\api\validate;

use think\Validate;
use data\model\Article as ArticleModel;

class order extends Validate
{
    protected $rule =   [
        'user'  => 'require',
        'new'  => 'require',
        'amount'   => 'require',
    ];

    protected $message  =   [
        'user.require'     => '用户标识不能为空',
        'new.require'     => '新闻标识不能为空',
        'amount.require'      => '价钱不能为空',
    ];

}