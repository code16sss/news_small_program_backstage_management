<?php

namespace app\api\controller;

\think\Loader::addNamespace('data', './data/');

use think\Controller;



class BaseApiController  extends Controller{

    protected function initialize()
    {
        $this->beforeActionList['validateAccessToken'] =['except'=>$this->dontValidateAccessToken];
        parent::initialize();
    }

    protected $dontValidateAccessToken = '';

    protected $beforeActionList = [
        'validateAccessToken'=>['except'=>[]]
    ];

    public function validateAccessToken(){
        $author = new AuthController();
        if(!$author->token_validate()){
            echo  json_encode([
                'status'=>1,
                'info'=>'access_token error'
            ]);
            die();
        }
    }

    /**
     * 返回特定格式
     * @params $status 返回状态值
     * @params $info 信息
     * @params @extar 额外信息 url 链接 data 返回数据 format 输出格式
     */
    protected function returnInfo($status,$info="",$extar=[],$format='json'){
        $data = [
            'status'=>$status,
            'info'=>$info,
        ];

        return $format(array_merge($data,$extar));
    }

    /**
     * 新版跳转
     * @param $status
     * @param string $info
     * @param array $extar
     * @param string $format
     * @return mixed
     * @author 16
     * @date 2018/2/8
     */
    protected function ajax($status,$info="",$extar=[],$format='json'){
        $data = [
            'status'=>$status,
            'info'=>$info,
            'data'=>$extar
        ];

        return $format($data);
    }
}