<?php

namespace app\api\controller\v1;

use think\facade\Session;
use think\Request;
use data\service\UserService;

class UserController extends \app\api\controller\BaseApiController
{

    private $user;

    public function initialize()
    {
        parent::initialize();
        $this->user = new UserService();
    }

    protected $dontValidateAccessToken = 'login,loginExit,updateAdminPsw';

    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        //
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        //
    }

    public function sendCode(Request $request)
    {
        $code = $request->get('code');
        $param = [
            'appid' => "wxbed5e4546c91a405",
            'secret' => "cc61d8aacec45d43cd9e4bfc2f49e049",
            'js_code' => $code,
            'grant_type' => 'authorization_code'
        ];
        $url = "https://api.weixin.qq.com/sns/jscode2session?appid={$param['appid']}&secret={$param['secret']}&js_code={$param['js_code']}&grant_type=authorization_code";
        $data = post_data ( $url, [ ] ); // {"session_key":"gHkoobsIWnTYnUj1ZTQKDA==","expires_in":2592000,"openid":"onfcX0fV3mjzsLRK7C15vk_2N86w"}

        if ((isset ( $data ['errcode'] ) && $data ['errcode'] == '40029') || ! isset ( $data ['session_key'] ) || ! isset ( $data ['openid'] )) {
            return $this->ajax(410001,$data['errmsg']);
        } else {
            return $this->ajax(0,'获取成功',['openid'=>$data['openid']]);
        }
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        $post = $request->except('iden');
        $validate = new \app\api\validate\user
        ;

        if (!$validate->scene('add')->check($post))
        {
            return $this->ajax(1,$validate->getError());
        }

        if (!$uid = $this->user->isExists($post['openid']))
        {
            $uid = $this->user->add($post);
        }else{
            $this->user->edit($uid,$post);
        }

        session('user',$uid);
        return $this->ajax(0,'更新成功',['uid'=>$uid]);
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //
    }

    /**
     * 登陆验证
     */
    public function login(Request $request)
    {
        $post = $request->only(['account','psw'], 'post');
        $validate = new \app\api\validate\user;

        if (!$validate->scene('validate')->check($post)) {
            return $this->ajax(1,$validate->getError());
        }

        if ($user = $this->user->checkUser($post)) {
            $this->user->setLoginStatus($user);
            return $this->ajax(0,'登陆成功',['url'=>url('/admin')]);
        }else {
            return $this->ajax(1,$this->user->getError());
        }
    }

    /**
     * 判断用户是否登陆
     * @desc
     * @return bool
     * @author 16
     * @date 2018/3/20
     */
    public static function checkLogin(){
        return Session::has('adminUser') ? Session::get('adminUser') :false;
    }

    /**
     * 退出登录
     * @desc
     * @author 16
     * @date 2018/3/20
     */
    public function loginExit(){
        Session::delete('adminUser');
        return $this->returnInfo(0,'退出成功',['url'=>url('/admin/login')]);
    }

    /**
     * 修改账号密码
     * @desc
     * @author 16
     * @date 2018/3/29
     */
    public function updateAdminPsw(Request $request){
        $post = $request->only('password');
        $post['password'] = md5($post['password']);

        $user = Session('adminUser');
        $this->user->edit($user['id'],$post);

        Session::delete('adminUser');

        return $this->returnInfo(0,'修改成功',['url'=>url('/admin/login')]);
    }
}
