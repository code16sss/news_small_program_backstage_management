<?php

namespace app\api\controller\v1;

use data\service\ContentService;
use think\Controller;
use think\Request;
use Config;
use app\api\controller\BaseApiController;
use data\service\NewService;


class NewsController extends BaseApiController
{
    private $news;

    protected function initialize()
    {
        parent::initialize();
        $this->news = new NewService();
    }

    protected $dontValidateAccessToken = 'save,update,delete';

    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index(Request $request)
    {
        $type = $request->get('type','all');
        $p = $request->get('p',1);
        $limit = $request->get('limit',15);

        list($count,$data) = array_values($this->news->getNewByPage($p,$limit,'id,type,cover,title,create_at',$type));

        return $this->ajax(1,'获取成功',[
            'count'=>$count,
            'news'=>$data,
            'page'=>$p,
            'size'=>$limit
        ]);
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        //
    }


    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        $post = $request->post();
        isset($post['is_top']) && $this->transFrom($post['is_top']);
        $validate = new \app\api\validate\news;
        if (!$validate->check($post))
        {
            return $this->ajax(1,$validate->getError());
        }

        $content = new ContentService();
        $post['content'] = $content->add(['content'=>$post['content']]);

        if ($this->news->add($post))
        {
            $url = url('/admin/news');
            return $this->returnInfo(0,"添加成功!",['url'=>$url]);
        }else{
            return $this->returnInfo(1,$this->news->getError());
        }
    }

    private function transFrom(&$attr){
        switch ($attr){
            case 'on':
                $attr = 1;
                break;
            case 'off':
                $attr = 0;
                break;
        }
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        if ($this->news->isExists($id) && !$this->news->needPay($id))
        {
            $data = $this->news->getById($id,'id,title,author,create_at,content',['content_text']);
            return $this->ajax(0,'获取成功',$data);
        }else{
            return $this->ajax(1,'未识别id');
        }
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        $post = $request->put();
        isset($post['is_top']) && $this->transFrom($post['is_top']);
        $validate = new \app\api\validate\news;
        if (!$validate->check($post))
        {
            return $this->ajax(1,$validate->getError());
        }

        $new = $this->news->getById($id);
        $content = new ContentService();
        $content->edit($new->content,['content'=>$post['content']]);
        $post['content'] = $new->content;

        if ($this->news->edit($id,$post))
        {
            $url = url('/admin/news');
            return $this->returnInfo(0,"编辑成功!",['url'=>$url]);
        }else{
            return $this->returnInfo(1,$this->news->getError());
        }
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        if ($this->news->delete($id))
        {
            return $this->returnInfo(0,"删除成功!",['url'=>url('/admin/news')]);
        }else{
            return $this->returnInfo(1,$this->news->getError());
        }
    }

    /**
     * 获取轮播内容
     * @desc    只会获取前三的轮播内容
     * @return mixed
     * @author 16
     * @date 2018/3/15
     */
    public function getTopLists(){
        $data['news'] = $this->news->getTopNews('id,cover,title');

        return $this->ajax(0,'获取成功',$data);
    }

    /**
     * 获取新闻分类
     * @desc
     * @author 16
     * @date 2018/3/25
     */
    public function getType(){
        return $this->news->getTypes();
    }
}
