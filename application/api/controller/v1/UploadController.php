<?php

namespace app\api\controller\v1;

use think\Controller;
use think\Request;
use app\api\controller\BaseApiController;
use data\service\ImgService;

class UploadController extends BaseApiController
{
    private $img;

    protected function initialize()
    {
        parent::initialize();
        $this->img = new ImgService();
    }

    protected $dontValidateAccessToken = 'uploadImg';

    /**
     * 上传图片到uploads目录
     * @desc
     * @author 16
     * @date 2018/2/8
     */
    public function uploadImg()
    {
        $file = request()->file('file');
        $info = $file->move( './public/upload');
        if ($info)
        {
            $url = '/public/upload/'.$info->getSaveName();
            if ($id=$this->img->saveImg($url))
            {
                $data = ['img_id'=>$id,'url'=>current_domain().'/public/upload/'.$info->getSaveName()];
            }

            return $this->ajax(0,'上传成功',$data);
        }else{
            $info = $file->getError();
            return $this->ajax(0,$info);
        }
    }

}
