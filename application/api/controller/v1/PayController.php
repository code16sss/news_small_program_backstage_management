<?php

namespace app\api\controller\v1;

use think\Controller;
use think\Request;
use app\api\controller\BaseApiController;
use data\service\NewService;
use data\service\ContentService;



class PayController extends BaseApiController
{
    private $news;

    protected $dontValidateAccessToken = 'save,update,delete';

    protected function initialize()
    {
        parent::initialize();
        $this->news = new NewService();
    }

    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index(Request $request)
    {

        $p = $request->get('p',1);
        $limit = $request->get('limit',15);

        list($count,$data) = array_values($this->news->getPayContentByPage($p,$limit,'id,cover,title'));

        return $this->ajax(1,'获取成功',[
            'count'=>$count,
            'news'=>$data,
            'page'=>$p,
            'size'=>$limit
        ]);
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        //
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        $post = $request->post();
        $validate = new \app\api\validate\pay;

        if (!$validate->check($post))
        {
            return $this->ajax(1,$validate->getError());
        }

        $content = new ContentService();
        $post['content'] = $content->add(['content'=>$post['content']]);

        isset($post['pay_content']) && $post['pay_content'] = $content->add(['content'=>$post['pay_content']]);

        if ($this->news->add($post))
        {
            $url = url('/admin/pay');
            return $this->returnInfo(0,"添加成功!",['url'=>$url]);
        }else{
            return $this->returnInfo(1,$this->news->getError());
        }
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read(Request $request, $id)
    {
        $openid = $request->get('user');
        if ($this->news->isExists($id) && $this->news->needPay($id))
        {
            if ($this->news->canSeeDetail($openid,$id))
            {
                $data = $this->news->getById($id,'id,title,author,create_at,content',['content_text']);
                $data['status'] = 1;
                return $this->ajax(0,'获取成功',$data);
            }else{
                $data = $this->news->getById($id,'id,author,title,create_at,pay_content,price,cover',['pay_content_text','cover_text']);
                $data['status'] = 0;
                return $this->ajax(0,'获取成功',$data);
            }

        }else{
            return $this->ajax(1,'未识别id');
        }
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        $post = $request->put();

        $validate = new \app\api\validate\pay;
        if (!$validate->check($post))
        {
            return $this->ajax(1,$validate->getError());
        }

        $new = $this->news->getById($id);
        $content = new ContentService();
        $content->edit($new->content,['content'=>$post['content']]);
        $post['content'] = $new->content;

        if ($this->news->edit($id,$post))
        {
            $url = url('/admin/pay');
            return $this->returnInfo(0,"编辑成功!",['url'=>$url]);
        }else{
            return $this->returnInfo(1,$this->news->getError());
        }
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        if ($this->news->delete($id))
        {
            return $this->returnInfo(0,"删除成功!",['url'=>url('/admin/payContent')]);
        }else{
            return $this->returnInfo(1,$this->news->getError());
        }
    }
     public function getPayUser($id){
        if ($this->news->isExists($id))
        {
            $data['user'] = $this->news->getPayUserById($id,'user');
            return $this->ajax(0,'获取成功',$data);
        }else{
            return $this->ajax(1,$this->news->getError());
        }
     }
}
