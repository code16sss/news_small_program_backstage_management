<?php

namespace app\api\controller\v1;

use app\api\controller\BaseApiController;
use data\service\OrderService;
use think\Controller;
use think\Request;

class OrderController extends BaseApiController
{

    /**
     * 支付成功之后的回调函数
     * @desc
     * @author 16
     * @date 2018/3/31
     */
    public function payCallback(Request $request){
        $post = $request->post();
        $validate = new \app\api\validate\order;
        if (!$validate->check($post))
        {
            return $this->ajax(1,$validate->getError());
        }

        $order = new OrderService();

        if ($order->add($post))
        {
            return $this->returnInfo(0,"支付成功!",[]);
        }else{
            return $this->returnInfo(1,$this->news->getError());
        }
    }
}
