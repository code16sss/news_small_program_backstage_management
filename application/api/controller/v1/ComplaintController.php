<?php

namespace app\api\controller\v1;

use think\Controller;
use think\Request;
use data\service\ComplaintService;

class ComplaintController extends \app\api\controller\BaseApiController
{

    private $complaint;

    protected $dontValidateAccessToken = 'update,delete';


    protected function initialize()
    {
        parent::initialize();
        $this->complaint = new ComplaintService();
    }

    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        //
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        //
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        $post = $request->post();
        $validate = new \app\api\validate\complaint;

        if (!$validate->check($post))
        {
            return $this->ajax(1,$validate->getError());
        }


        if($this->complaint->add($post)){
            return $this->ajax(0,'发布成功!');
        }else{
            return $this->ajax(1,$this->complaint->getError());
        }
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        $post = $request->put();

        if ($this->complaint->edit($id,$post)) {
            $url = url('/admin/complaint');
            return $this->returnInfo(0,"编辑成功!",['url'=>$url]);
        }else {
            return $this->returnInfo(1,'请修改内容');
        }
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        if ($this->complaint->delete($id))
        {
            return $this->returnInfo(0,"删除成功!",['url'=>url('/admin/complaint')]);
        }else{
            return $this->returnInfo(1,$this->complaint->getError());
        }
    }

}
