<?php

namespace app\admin\controller;

use think\Controller;
use think\Request;
use data\service\NewService;

class NewController extends BaseController
{
    private $news;

    protected function initialize()
    {
        parent::initialize();
        $this->news = new NewService();
    }
    /**
     * 新闻首页
     * @desc
     * @return mixed
     * @author 16
     * @date 2018/3/21
     */
    public function index(){
        $this->assign('types',$this->news->getTypes());
        return $this->fetch();
    }

    /**
     * 添加新闻
     * @desc
     * @author 16
     * @date 2018/3/21
     */
    public function add(){
        $this->assign('types',$this->news->getTypes());
        return $this->fetch();
    }

    /**
     * 编辑新闻
     * @desc
     * @param $id
     * @return mixed
     * @author 16
     * @date 2018/3/23
     */
    public function edit($id){
        $new = $this->news->getByIdToEdit($id);
        $this->assign('data',$new);
        $this->assign('id',$id);
        $this->assign('types',$this->news->getTypes());
        return $this->fetch();
    }

    public function getNews(Request $request){
        $page = $request->get('page',1);
        $size = $request->get('size',8);
        $extra['append'] = ['type_text'];
        $request->get('type') && $extra['where'][] = ['type','=',$request->get('type')];
        $request->get('key') && $extra['where'][] =['title','like','%'.$request->get('key').'%'];
        $extra['where'][] =['price','=',0];

        list($count,$data) = array_values($this->news->getByPage($page,$size,'*',$extra));
        return $this->ajax(1,'',[
            'count'=>$count,
            'data'=>$data,
            'page'=>$page,
            'size'=>$size
        ]);
    }
}
