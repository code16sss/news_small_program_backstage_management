<?php

namespace app\admin\controller;

use think\Request;
use data\service\ComplaintService;

class ComplaintController extends BaseController
{

    private $complaint;

    protected function initialize()
    {
        parent::initialize();
        $this->complaint = new ComplaintService();
    }

    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {

        return $this->fetch();
    }

    /**
     * 爆料详情
     * @desc
     * @author 16
     * @date 2018/3/25
     */
    public function detail($id){
        $content = $this->complaint->getByIdToEdit($id);

        $this->assign('data',$content);
        $this->assign('id',$id);
        return $this->fetch();

    }

    public function getComplaint(Request $request){
        $page = $request->get('page',1);
        $size = $request->get('size',8);
        $extra = [];
        list($count,$data) = array_values($this->complaint->getByPage($page,$size,'*',$extra));
        return $this->ajax(1,'',[
            'count'=>$count,
            'data'=>$data,
            'page'=>$page,
            'size'=>$size
        ]);
    }
}
