<?php

namespace app\admin\controller;

use think\Controller;
use think\Request;
use data\service\OrderService;

class OrderController extends BaseController
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index(Request $request)
    {
        $this->assign('uid',$request->get('uid',0));
        $this->assign('pid',$request->get('pid',0));
        return $this->fetch();
    }

    public function getOrder(Request $request){
        $page = $request->get('page',1);
        $size = $request->get('size',8);
        $extra = [];
        $request->get('pid') && $extra['where'][] = ['new','=',$request->get('pid')];
        $request->get('uid') && $extra['where'][] =['user','=',$request->get('uid')];

        $service = new OrderService();
        list($count,$data) = array_values($service->getByPage($page,$size,'*',$extra));
        return $this->ajax(1,'',[
            'count'=>$count,
            'data'=>$data,
            'page'=>$page,
            'size'=>$size
        ]);
    }
}
