<?php

namespace app\admin\controller;

use think\Request;
use data\service\NewService;

class PayController extends BaseController
{
    private $news;

    protected function initialize()
    {
        parent::initialize();
        $this->news = new NewService();
    }
    /**
     * 支付内容首页
     * @desc
     * @author 16
     * @date 2018/3/24
     */
    public function index(){
        return $this->fetch();
    }

    /**
     * 添加支付内容
     * @desc
     * @author 16
     * @date 2018/3/24
     */
    public function add(){
        return $this->fetch();
    }

    public function getPay(Request $request){
        $page = $request->get('page',1);
        $size = $request->get('size',8);
        $extra['append'] = ['pay_num'];

        $request->get('key') && $extra['where'][] =['title','like','%'.$request->get('key').'%'];
        $extra['where'][] =['price','>',0];

        list($count,$data) = array_values($this->news->getByPage($page,$size,'*',$extra));
        return $this->ajax(1,'',[
            'count'=>$count,
            'data'=>$data,
            'page'=>$page,
            'size'=>$size
        ]);
    }

    /**
     * 编辑支付内容
     * @desc
     * @param $id
     * @return mixed
     * @author 16
     * @date 2018/3/24
     */
    public function edit($id){
        $new = $this->news->getByIdToEdit($id);

        $this->assign('data',$new);
        $this->assign('id',$id);

        return $this->fetch();
    }
}
