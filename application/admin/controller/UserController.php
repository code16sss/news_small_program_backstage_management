<?php

namespace app\admin\controller;

use think\Controller;
use think\Request;
use data\service\UserService;

class UserController extends BaseController
{
    /**
     * 用户管理
     *
     * @return \think\Response
     */
    public function index()
    {
        return $this->fetch();
    }

    public function getUser(Request $request){
        $page = $request->get('page',1);
        $size = $request->get('size',8);
        $extra['append'] = ['home'];
        $request->get('key') && $extra['where'][] =['nickname','like','%'.$request->get('key').'%'];
        $extra['where'][] = ['iden','=',2];
        $userModel = new UserService();

        list($count,$data) = array_values($userModel->getByPage($page,$size,'*',$extra));
        return $this->ajax(1,'',[
            'count'=>$count,
            'data'=>$data,
            'page'=>$page,
            'size'=>$size
        ]);
    }


}
