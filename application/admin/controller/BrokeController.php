<?php

namespace app\admin\controller;

use think\Request;
use data\service\BrokeService;

class BrokeController extends BaseController
{

    private $brokeService;

    protected function initialize()
    {
        parent::initialize();
        $this->brokeService = new BrokeService();
    }

    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {

        return $this->fetch();
    }

    /**
     * 爆料详情
     * @desc
     * @author 16
     * @date 2018/3/25
     */
    public function detail($id){
        $content = $this->brokeService->getByIdToEdit($id);

        $this->assign('data',$content);
        $this->assign('id',$id);
        return $this->fetch();

    }

    public function getBroke(Request $request){
        $page = $request->get('page',1);
        $size = $request->get('size',8);
        $extra = [];
        $request->get('status') && $extra['where'][] = ['status','=',$request->get('status')];
        list($count,$data) = array_values($this->brokeService->getByPage($page,$size,'*',$extra));
        return $this->ajax(1,'',[
            'count'=>$count,
            'data'=>$data,
            'page'=>$page,
            'size'=>$size
        ]);
    }
}
