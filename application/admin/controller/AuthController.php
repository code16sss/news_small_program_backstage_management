<?php

namespace app\admin\controller;

use think\Controller;
use think\Request;

class AuthController extends Controller
{
    /**
     * 登陆界面
     * @desc
     * @author 16
     * @date 2018/3/20
     */
    public function login(){
        return $this->fetch();
    }
}
