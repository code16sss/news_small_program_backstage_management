<?php

namespace app\admin\controller;

use think\Controller;
use think\Request;
use app\api\controller\v1\UserController as User;

class BaseController extends Controller
{
    protected $beforeActionList = [
        'isLogin'
    ];

    protected function isLogin(){
        if($userInfo = User::checkLogin()){
            $this->user = $userInfo;
            $this->assign('user',$userInfo);
        }else{
            $this->redirect(url('/admin/login'));
        }
    }

    protected function ajax($status,$info="",$extar=[],$format='json'){
        $data = [
            'status'=>$status,
            'info'=>$info,
            'data'=>$extar
        ];

        return $format($data);
    }
}
