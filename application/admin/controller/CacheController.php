<?php

namespace app\admin\controller;

use think\Controller;
use think\Request;
use think\facade\Cache;

class CacheController extends BaseController
{
    public function clear(){
        Cache::clear();
        return $this->ajax(0,'清除成功!');
    }
}
