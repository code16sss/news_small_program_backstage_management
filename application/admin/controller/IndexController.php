<?php

namespace app\admin\controller;

use think\Controller;
use think\Request;
use data\service\UserService;
use data\service\NewService;
use data\service\BrokeService;
use data\service\OrderService;

class IndexController extends BaseController
{
    public function index(){
        /** 获取相应信息记载 */
        //用户上次登陆信息
        $prevLoginInfo = session('prevLoginInfo');
        $this->assign('loginInfo',$prevLoginInfo);

        //获取统计信息
        $totalInfo = $this->getCountInfo();
        $this->assign('totalInfo',$totalInfo);

        return $this->fetch();
    }

    /**
     * 获取相应的统计信息
     * @desc
     * @author 16
     * @date 2018/3/20
     */
    public function getCountInfo(){
        $user = new UserService();
        $new = new NewService();
        $broke = new BrokeService();
        $order = new OrderService();

        /* 总用户数 */
        $userCount = $user->getUserCount();
        $todayFollowUserCount = $user->getTodayFollowCount();
        $newsCount =$new->getNewCount();
        $payContentCount =$new->getPayContentCount();
        $brokeCount =$broke->getBrokeCount();
        $moneySum =$order->getMoneySum();

        return [
            'userCount'=>$userCount,
            'todayFollowUserCount'=>$todayFollowUserCount,
            'newsCount'=>$newsCount,
            'payContentCount'=>$payContentCount,
            'brokeCount'=>$brokeCount,
            'moneySum'=>$moneySum,
        ];
    }
}
