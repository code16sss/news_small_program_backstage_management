var url = document.location.toString();
var arrUrl = url.split("?");
var baseUrl = arrUrl[0]+'?s=/';
function urlAjax(url,data,success_func,error_func,method){
    if(!method) method = 'get';
    $.ajax({
        type: method,
        url: url,
        data: data,
        dataType: "json",
        success: function(data){
            success_func(data);
        },
        error : function(){
            error_func();
        }
    });
}