
layui.define(['laypage', 'layer', 'form', 'pagesize'], function (exports) {
    var $ = layui.jquery,
        layer = layui.layer,
        form = layui.form(),
        laypage = layui.laypage;
    var laypageId = 'pageNav';

    var currentIndex = 1;
    var pageSize = 8;
    initilData(currentIndex, pageSize);
    //页数据初始化
    //currentIndex：当前也下标
    //pageSize：页容量（每页显示的条数）
    function initilData(currentIndex, pageSize) {
        var index = layer.load(1);
        var data = {page:currentIndex,size:pageSize,status:$("[name=status]").val()}
        urlAjax(baseUrl+'admin/getComplaint',data,function(data){
            layer.close(index);
            var news = data.data.data;
            pages = Math.ceil(data.data.count/pageSize);
            var html = '';  //由于静态页面，所以只能作字符串拼接，实际使用一般是ajax请求服务器数据
            html += '<table style="" class="layui-table" lay-even>';
            html += '<colgroup><col width="180"><col><col width="150"><col width="180"><col width="90"><col width="90"><col width="50"><col width="50"></colgroup>';
            html += '<thead><tr><th>发表时间</th><th>内容</th><th>联系方式</th><th>地址</th><th>用户</th><th colspan="1">操作</th></tr></thead>';
            html += '<tbody>';
            for (var i = 0; i < news.length; i++) {
                var item = news[i];
                var options = item.option;

                html += "<tr>";
                html += "<td>" + item.create_at + "</td>";
                html += "<td>" + item.content + "</td>";
                html += "<td>" + item.mobile + "</td>";
                html += "<td>" + item.address + "</td>";
                html += "<td>" + item.username + "</td>";
                html += '<td><button class="layui-btn layui-btn-small layui-btn-danger" onclick="layui.datalist.deleteData(\'' + item.id + '\')"><i class="layui-icon">&#xe640;</i></button></td>';
                html += "</tr>";
            }
            html += '</tbody>';
            html += '</table>';
            html += '<div id="' + laypageId + '"></div>';
            $('#dataContent').html(html);
            form.render('checkbox');  //重新渲染CheckBox，编辑和添加的时候
            laypage({
                cont: laypageId,
                pages: pages,
                groups: data.data.size,
                skip: true,
                curr: data.data.page,
                jump: function (obj, first) {
                    var currentIndex = obj.curr;
                    if (!first) {
                        initilData(currentIndex, pageSize);
                    }
                }
            });
        }, function(){
            layer.close(index);
            layer.msg('网络错误，请重试', { icon: 5 });
        });

    }


    //输出接口，主要是两个函数，一个删除一个编辑
    var datalist = {
        deleteData: function (id) {
            layer.confirm('确定删除？', {
                btn: ['确定', '取消'] //按钮
            }, function () {
                var index = layer.load(1);
                urlAjax(baseUrl+'api/v1/complaint/'+id,[],function(data){
                    layer.close(index);
                    if(data.status){
                        layer.msg(data.info, { icon: 5 });
                    }else{
                        layer.msg(data.info, { icon: 6 });
                        initilData(currentIndex, pageSize);
                    }
                }, function(){
                    layer.close(index);
                    layer.msg('网络错误，请重试', { icon: 5 });
                },'DELETE');
            }, function () {

            });
        },
    };


    exports('datalist', datalist);
});