
layui.define(['laypage', 'layer', 'form', 'pagesize'], function (exports) {
    var $ = layui.jquery,
        layer = layui.layer,
        form = layui.form(),
        laypage = layui.laypage;
    var laypageId = 'pageNav';

    var currentIndex = 1;
    var pageSize = 8;
    initilData(currentIndex, pageSize);
    //页数据初始化
    //currentIndex：当前也下标
    //pageSize：页容量（每页显示的条数）
    function initilData(currentIndex, pageSize) {
        var index = layer.load(1);
        var data = {page:currentIndex,size:pageSize,key:$("[name=key]").val()}
        urlAjax(baseUrl+'admin/getUser',data,function(data){
            layer.close(index);
            var news = data.data.data;
            pages = Math.ceil(data.data.count/pageSize);
            var html = '';  //由于静态页面，所以只能作字符串拼接，实际使用一般是ajax请求服务器数据
            html += '<table style="" class="layui-table" lay-even>';
            html += '<colgroup><col width="180"><col><col width="150"><col width="180"><col width="90"><col width="90"><col width="50"><col width="50"></colgroup>';
            html += '<thead><tr><th>关注时间</th><th>头像</th><th>昵称</th><th>性别</th><th>所在地</th><th colspan="3">操作</th></tr></thead>';
            html += '<tbody>';
            for (var i = 0; i < news.length; i++) {
                var item = news[i];
                var options = item.option;

                html += "<tr>";
                html += "<td>" + item.create_at + "</td>";
                html += "<td> <img src='" + item.avatarurl + "' style='width:60px;height:60px;border-radius: 50%;'/></td>";
                html += "<td>" + item.nickname + "</td>";
                html += "<td>" + item.gender + "</td>";
                html += "<td>" + item.home + "</td>";
                // html += '<td><button class="layui-btn layui-btn-small layui-btn-normal" onclick="layui.datalist.editData(\'' + item.id + '\')"><i class="layui-icon">&#xe642;</i></button></td>';
                // html += '<td><button class="layui-btn layui-btn-small layui-btn-danger" onclick="layui.datalist.deleteData(\'' + item.id + '\')"><i class="layui-icon">&#xe640;</i></button></td>';
                html += '<td><button class="layui-btn layui-btn-small layui-btn-danger" onclick="layui.datalist.payData(\'' + item.id + '\')"><i class="layui-icon">&#xe62c;</i></button></td>';
                html += "</tr>";
            }
            html += '</tbody>';
            html += '</table>';
            html += '<div id="' + laypageId + '"></div>';
            $('#dataContent').html(html);
            form.render('checkbox');  //重新渲染CheckBox，编辑和添加的时候
            laypage({
                cont: laypageId,
                pages: pages,
                groups: data.data.size,
                skip: true,
                curr: data.data.page,
                jump: function (obj, first) {
                    var currentIndex = obj.curr;
                    if (!first) {
                        initilData(currentIndex, pageSize);
                    }
                }
            });
        }, function(){
            layer.close(index);
            layer.msg('网络错误，请重试', { icon: 5 });
        });

    }

    //修改选项
    function setOption(id,option,ele){
        var index = layer.load(1);
        urlAjax(baseUrl+'api/updateField/article/'+id,{'option':option},function(data){
            layer.close(index);
            if(data.status){
                layer.msg(data.info, { icon: 5 });
                if (ele.elem.checked) {
                    ele.elem.checked = false;
                }
                else {
                    ele.elem.checked = true;
                }
            }else{

                layer.msg(data.info, { icon: 6 });
            }
            form.render();  //重新渲染
        }, function(){
            layer.close(index);
            if (ele.elem.checked) {
                ele.elem.checked = true;
            }
            else {
                ele.elem.checked = false;
            }
            form.render();  //重新渲染
            layer.msg('网络错误，请重试', { icon: 5 });
        },'PUT');
    }



    //监听标签下拉
    form.on('select(formLabel)', function(data){
        initilData(currentIndex, pageSize);
        return false;
    });

    //监听搜索提交
    form.on('submit(formSearch)', function (data) {
        initilData(currentIndex, pageSize);
        return false;
    });



    //添加数据
    $('#addArticle').click(function () {
        var index = layer.load(1);
        var url = $(this).attr('data-url')
        setTimeout(function () {
            layer.close(index);
            location.href = url;
        }, 500);
    });

    //输出接口，主要是两个函数，一个删除一个编辑
    var datalist = {
        deleteData: function (id) {
            layer.confirm('确定删除？', {
                btn: ['确定', '取消'] //按钮
            }, function () {
                var index = layer.load(1);
                urlAjax(baseUrl+'api/v1/new/'+id,[],function(data){
                    layer.close(index);
                    if(data.status){
                        layer.msg(data.info, { icon: 5 });
                    }else{
                        layer.msg(data.info, { icon: 6 });
                        initilData(currentIndex, pageSize);
                    }
                }, function(){
                    layer.close(index);
                    layer.msg('网络错误，请重试', { icon: 5 });
                },'DELETE');
            }, function () {

            });
        },
        editData: function (id) {
            var url = baseUrl+'admin/editPay/'+id;
            location.href = url;
        },
        payData: function (id) {
            var url = baseUrl+'admin/order&uid='+id;
            location.href = url;
        }
    };


    exports('datalist', datalist);
});