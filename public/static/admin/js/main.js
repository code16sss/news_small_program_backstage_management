﻿

layui.define(['element', 'layer', 'util', 'pagesize', 'form'], function (exports) {
    var $ = layui.jquery;
    var element = layui.element();
    var layer = layui.layer;
    var util = layui.util;
    var form = layui.form();
    //form.render();
    //快捷菜单开关
    $('span.sys-title').click(function (e) {
        e.stopPropagation();    //阻止事件冒泡
        $('div.short-menu').slideToggle('fast');
    });
    $('div.short-menu').click(function (e) {
        e.stopPropagation();    //阻止事件冒泡
    });
    $(document).click(function () {
        $('div.short-menu').slideUp('fast');
        $('.individuation').removeClass('bounceInRight').addClass('flipOutY');
    });
    //个性化设置开关
    $('#individuation').click(function (e) {
        e.stopPropagation();    //阻止事件冒泡
        $('.individuation').removeClass('layui-hide').toggleClass('bounceInRight').toggleClass('flipOutY');
    });
    $('.individuation').click(function (e) {
        e.stopPropagation();    //阻止事件冒泡
    })
    $('.layui-body').click(function () {
        $('.individuation').removeClass('bounceInRight').addClass('flipOutY');
    });

    //监听左侧导航点击
    element.on('nav(leftnav)', function (elem) {
        var url = $(elem).children('a').attr('data-url');   //页面url
        var id = $(elem).children('a').attr('data-id');     //tab唯一Id
        var title = $(elem).children('a').text();           //菜单名称
        if (title == "首页") {
            element.tabChange('tab', 0);
            return;
        }
        if (url == undefined) return;

        var tabTitleDiv = $('.layui-tab[lay-filter=\'tab\']').children('.layui-tab-title');
        var exist = tabTitleDiv.find('li[lay-id=' + id + ']');
        if (exist.length > 0) {
            //切换到指定索引的卡片
            element.tabChange('tab', id);
        } else {
            var index = layer.load(1);
            //由于Ajax调用本地静态页面存在跨域问题，这里用iframe
            setTimeout(function () {
                //模拟菜单加载
                layer.close(index);
                element.tabAdd('tab', { title: title, content: '<iframe src="' + url + '" style="width:100%;height:100%;border:none;outline:none;"></iframe>', id: id });
                //切换到指定索引的卡片
                element.tabChange('tab', id);
            }, 500);
        }
    });

    //监听快捷菜单点击
    $('.short-menu .layui-field-box>div>div').click(function () {
        var elem = this;
        var url = $(elem).children('span').attr('data-url');
        var id = $(elem).children('span').attr('data-id');
        var title = $(elem).children('span').text();

        if (url == undefined) return;

        var tabTitleDiv = $('.layui-tab[lay-filter=\'tab\']').children('.layui-tab-title');
        var exist = tabTitleDiv.find('li[lay-id=' + id + ']');
        if (exist.length > 0) {
            //切换到指定索引的卡片
            element.tabChange('tab', id);
        } else {
            var index = layer.load(1);
            //由于Ajax调用本地静态页面存在跨域问题，这里用iframe
            setTimeout(function () {
                //模拟菜单加载
                layer.close(index);
                element.tabAdd('tab', { title: title, content: '<iframe src="' + url + '" style="width:100%;height:100%;border:none;outline:none;"></iframe>', id: id });
                //切换到指定索引的卡片
                element.tabChange('tab', id);
            }, 500);
        }
        $('div.short-menu').slideUp('fast');
    });

    //监听侧边导航开关
    form.on('switch(sidenav)', function (data) {
        if (data.elem.checked) {
            showSideNav();
            layer.msg('这个开关是layui的开关改编的');
        } else {
            hideSideNav();
        }
    });

    //收起侧边导航点击事件
    $('.layui-side-hide').click(function () {
        hideSideNav();
        $('input[lay-filter=sidenav]').siblings('.layui-form-switch').removeClass('layui-form-onswitch');
        $('input[lay-filter=sidenav]').prop("checked", false);
    });

    //鼠标靠左展开侧边导航
    $(document).mousemove(function (e) {
        if (e.pageX == 0) {
            showSideNav();
            $('input[lay-filter=sidenav]').siblings('.layui-form-switch').addClass('layui-form-onswitch');
            $('input[lay-filter=sidenav]').prop("checked", true);
        }
    });

//注销当前账号
    $('#clear').click(function(){
        var index = layer.load(1);
        url = baseUrl+'admin/clearcache';
        urlAjax(url,{},function(data){
            layer.close(index);
            layer.msg(data.info, { icon: 6 });
        },function(){
            layer.close(index);
            layer.msg('网络错误，请重试!', { icon: 5 });
        });
    });

    //修改用户密码
    $('#user').click(function(){
        //页面层
        layer.open({
            type: 1,
            title:'修改管理员密码',
            skin: 'layui-layer-rim', //加上边框
            area: ['420px', '240px'], //宽高
            content: '<form class="layui-form" action=""><div class="layui-form-item">\n' +
            '    <label class="layui-form-label">密码框</label>\n' +
            '    <div class="layui-input-inline">\n' +
            '      <input type="password" name="password" required lay-verify="required" placeholder="请输入密码" autocomplete="off" class="layui-input">\n' +
            '    </div><div class="layui-form-item" >\n' +
            '    <label class="layui-form-label">密码框</label>\n' +
            '    <div class="layui-input-inline">\n' +
            '      <input type="password" name="againpassword" required lay-verify="required" placeholder="再次输入密码" autocomplete="off" class="layui-input">\n' +
            '    </div> <div class="layui-form-item">\n' +
            '    <div class="layui-input-block" >\n' +
            '      <button class="layui-btn" lay-submit lay-filter="edit" style="margin-top:15px;">修改密码</button>\n' +
            '    </div>\n' +
            '  </div></form>'
        });
    })
    //注销当前账号
    $('#exit').click(function(){
        //询问框
        layer.confirm('确定离开?', {
            btn: ['是','否'] //按钮
        }, function(){
            var index = layer.load(1);
            url = baseUrl+'api/v1/exit';
            urlAjax(url,{},function(data){
                layer.close(index);
                layer.msg('退出成功!', { icon: 6 });
                layer.closeAll('page');
                setTimeout(function(){
                    top.location.href = data.url;
                },1500);
            },function(){
                layer.close(index);
                layer.msg('网络错误，请重试!', { icon: 5 });
            });
        }, function(){

        });
    });

    //皮肤切换
    $('.skin').click(function () {
        var skin = $(this).attr("data-skin");
        $('body').removeClass();
        $('body').addClass(skin);
    });

    var ishide = false;
    //隐藏侧边导航
    function hideSideNav() {
        if (!ishide) {
            $('.layui-side').animate({ left: '-200px' });
            $('.layui-side-hide').animate({ left: '-200px' });
            $('.layui-body').animate({ left: '0px' });
            $('.layui-footer').animate({ left: '0px' });
            var tishi = layer.msg('鼠标靠左自动显示菜单', { time: 1500 });
            layer.style(tishi, {
                top: 'auto',
                bottom: '50px'
            });
            ishide = true;
        }
    }
    //显示侧边导航
    function showSideNav() {
        if (ishide) {
            $('.layui-side').animate({ left: '0px' });
            $('.layui-side-hide').animate({ left: '0px' });
            $('.layui-body').animate({ left: '200px' });
            $('.layui-footer').animate({ left: '200px' });
            ishide = false;
        }
    }

    function redirect(){
        alert(2333);
    }

    //修改密码
    form.on('submit(edit)', function(data){
        var index = layer.load(1);
        var psw = data.field.password;
        var repsw = data.field.againpassword;
        if(psw != repsw){
            layer.close(index);
            layer.msg('密码不一致，请重新输入', { icon: 5 });
        }else{
            urlAjax(baseUrl+'api/v1/updateAdminPsw',{password:psw},function(data){
                layer.close(index);
                if(data.status){
                    layer.msg(data.info, { icon: 5 });
                }else{
                    layer.msg(data.info, { icon: 6 });
                    layer.closeAll('page');
                    setTimeout(function(){
                        location.href = data.url;
                    },1500);
                }
            }, function(){
                layer.close(index);
                layer.msg('网络错误，请重试', { icon: 5 });
            },'POST');
        }


       return false;
    });



    exports('main', {});
});
