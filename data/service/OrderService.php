<?php
namespace data\service;

use data\model\Order;
use think\facade\Session;
class OrderService extends BaseService{

    // 模型初始化
    public function __construct()
    {
        $this->model = new Order;
    }

    public function getMoneySum(){
        return Order::sum('amount');
    }

}