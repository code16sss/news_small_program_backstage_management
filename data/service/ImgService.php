<?php
namespace data\service;

use data\model\Imgs;
use think\facade\Session;
class ImgService extends BaseService{

    // 模型初始化
    public function __construct()
    {
        $this->model = new Imgs;
    }

    /**
     * 保存链接
     * @desc
     * @author 16
     * @date 2018/3/14
     */
    public function saveImg($url){
        $this->model->save(['url'=>$url]);
        return $this->model->id;
    }
}