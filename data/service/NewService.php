<?php
namespace data\service;

use data\model\News;
use data\model\Imgs;
use data\model\Order;
use data\model\User;
use think\facade\Session;
class NewService extends BaseService{

    // 模型初始化
    public function __construct()
    {
        $this->model = new News;
    }

    /**
     * 返回轮播内容
     * @desc    只能返回前三条
     * @author 16
     * @date 2018/3/14
     */
    public function getTopNews($field){
        $news = News::scope('top')->field($field)->limit(3)->order('id DESC')->select()->append(['cover_text'])->toArray();

        return $news;
    }

    /**
     * 根据分类获取新闻分页
     * @desc
     * @param $page
     * @param $size
     * @param string $field
     * @param $type
     * @return array
     * @author 16
     * @date 2018/3/14
     */
    public function getNewByPage($page, $size,$field='*',$type){
        $extra['where'][] = ['price','=',0];
        $extra['order'] = 'id DESC';
        $type == 'all' || $extra['where'][] = ['type','=',$type];
        $extra['append'] = ['type_text','cover_text'];
        return $this->getByPage($page, $size,$field,$extra);
    }

    /**
     * 获取分页付费列表
     * @desc
     * @param $page
     * @param $size
     * @param string $field
     * @return array
     * @author 16
     * @date 2018/3/14
     */
    public function getPayContentByPage($page, $size,$field='*'){
        $extra['where'][] = ['price','>',0];
        $extra['append'] = ['cover_text'];
        return $this->getByPage($page, $size,$field,$extra);
    }

    /**
     * 用户是否可以直接查看内容
     * @desc
     * @author 16
     * @date 2018/3/14
     */
    public function canSeeDetail($openid,$id){
        /*是否需要付费*/
        if ($this->model->where('id','=',$id)->value('price'))
        {
            /** 是否已经付费*/
            $uid = User::where('openid','=',$openid)->value('id');
            return Order::where('user','=',$uid)->where('new','=',$id)->value('user');
        }else{
            return  true;
        }
    }

    /**
     * 获取付费内容的付费用户
     * @desc    每次都必须获取用户头像
     * @author 16
     * @date 2018/3/15
     */
    public function getPayUserById($id,$field='*'){
        return Order::field($field)->where('new','=',$id)->select()->append(['cover']);
    }

    /**
     * 判断是否需要付费
     * @desc
     * @param $id
     * @author 16
     * @date 2018/3/15
     */
    public function needPay($id){
        return $this->model->where('id','=',$id)->value('price')>0 ? true : false;
    }

    /**
     * 获取新闻总数
     * @desc
     * @author 16
     * @date 2018/3/20
     */
    public function getNewCount(){
        return News::scope('new')->count();
    }

    /**
     * 获取付费内容总数
     * @desc
     * @return int|string
     * @author 16
     * @date 2018/3/20
     */
    public function getPayContentCount(){
        return News::scope('pay')->count();
    }

    /**
     * 获取新闻分类
     * @desc
     * @author 16
     * @date 2018/3/22
     */
    public function getTypes(){
        return $this->model->getNewType();
    }




}