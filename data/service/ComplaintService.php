<?php
namespace data\service;

use data\model\Complaint;
use think\facade\Session;
class ComplaintService extends BaseService{

    // 模型初始化
    public function __construct()
    {
        $this->model = new Complaint;
    }

    public function getComplaintCount(){
        return Complaint::count();
    }


}