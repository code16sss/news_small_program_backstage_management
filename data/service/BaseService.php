<?php
namespace data\service;

class BaseService{
    protected $model;
    protected $errMsg = '网络错误，请重试';

    /**
     * 输出错误信息
     * @author 16
     * @author 2018-1-19
     */
    public function getError(){
        return $this->errMsg;
    }

    /**
     * 获取分页数据
     */
    public function getByPage($page,$size,$field='*',$extra){
        return [
            'count'=>$this->model->getInfoCount($extra),
            'data'=>$this->model->getInfoByPage($page,$size,$field,$extra)
        ];
    }

    /**
     * 新添内容
     */
    public function add($data){
        $data['create_at'] = time();
        $this->model->save($data);
        return $this->model->id;
    }

    /**
     * 通过id获取内容
     * @date 2018-1-30
     */
    public function getById($id,$field='*',$append=[]){
        return $this->model->field($field)->find($id)->append($append);
    }

    public function getByIdToEdit($id,$field='*'){
        return $this->model->field($field)->find($id);
    }

    /**
     * 编辑内容
     */
    public function edit($id,$data){
        $data['update_at'] = time();
        $this->model->save($data,['id'=>$id]);
        return $id;
    }

    /**
     * 删除内容
     */
    public function delete($id){
        $model = $this->model;
        if($model::destroy($id)){
            return true;
        }
    }

    /**
     * 判断文章标识是否正确
     * @desc
     * @param $id
     * @return mixed
     * @author 16
     * @date 2018/3/14
     */
    public function isExists($id){
        if($this->model->where('id','=',$id)->value('id')){
            return true;
        }else{
            $this->errMsg = '未识别id';
            return false;
        }
    }







}