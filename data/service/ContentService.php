<?php
namespace data\service;

use data\model\Contents;
use think\facade\Session;
class ContentService extends BaseService{

    // 模型初始化
    public function __construct()
    {
        $this->model = new Contents;
    }

    /**
     * 新添内容
     */
    public function add($data){
        $content = Contents::create($data);
        return $content->id;
    }

}