<?php
namespace data\service;

use data\model\Broke;
use think\facade\Session;
class BrokeService extends BaseService{

    // 模型初始化
    public function __construct()
    {
        $this->model = new Broke;
    }

    public function getBrokeCount(){
        return Broke::count();
    }


}