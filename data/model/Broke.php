<?php
namespace data\model;

class Broke extends BaseModel{

    const STATUS_UNSEE = 1; //未查看;
    const STATUS_USERFUL = 2; //有用的;
    const STATUS_UNUSERFUL = 3; //没用的;

    public function getBrokeType(){
        return [
            self::STATUS_USERFUL=>'有用的',
            self::STATUS_UNUSERFUL=>'无用的',
        ];
    }
    protected function setUserAttr($value)
    {
        return User::where('openid','=',$value)->value('id');
    }


    public function getUserAttr($value)
    {
        return User::where('id',$value)->value('nickname');
    }

    public function getImgsAttr($value)
    {
        $imgIds = json_decode($value,true);
        return Imgs::whereIn('id',$imgIds)->column('url');
    }

    public function getContentAttr($value)
    {
        return Contents::where('id',$value)->value('content');
    }

}