<?php
namespace data\model;

class News extends BaseModel{
    /*新闻分类*/
    const TYPE_TOUTIAO = 1;
    const TYPE_XINSHIDAI = 2;
    const TYPE_PANYU = 3;
    const TYPE_REDIAN = 4;
    const TYPE_XUETANG= 5;
    const TYPE_WEISHI= 6;
    const TYPE_WENHUA= 7;
    const TYPE_HUODONG= 8;
    const TYPE_XINMEITI= 9;
    const TYPE_WENZHENG= 10;
    const TYPE_DUBAO= 11;


    protected $autoWriteTimestamp = true;

    // 定义时间戳字段名
    protected $createTime = 'create_at';
    protected $updateTime = 'update_at';

    protected $auto = ['author'];

    public function scopeTop($query)
    {
        $where[] = ['price','=',0];
        $where[] = ['is_top','=',1];
        $query->where($where);
    }

    public function scopeNew($query)
    {
        $query->where('price',0);
    }

    public function scopePay($query){
        $query->where('price','>',0);
    }

    public function getCoverTextAttr($value,$data)
    {
        $img = Imgs::where('id','=',$data['cover'])->find();
        return $img->url;
    }

    public function getContentTextAttr($value,$data)
    {
        return Contents::where('id','=',$data['content'])->value('content');
    }

    public function getPaycontentTextAttr($value,$data)
    {
        return Contents::where('id','=',$data['pay_content'])->value('content');
    }

    public function getAuthorAttr($value)
    {
        return User::where('id','=',$value)->value('nickname');
    }

    public function getTypeTextAttr($value,$data)
    {
        return $this->getNewType()[$data['type']];
    }

    public function getPayNumAttr($value,$data)
    {
        return Order::where(['new'=>$data['id']])->count();
    }



    protected function setAuthorAttr()
    {
        return session('adminUser')['id'];
    }

    /**
     * 获取新闻分类
     * @desc
     * @return array
     * @author 16
     * @date 2018/3/22
     */
    public function getNewType(){
        return [
            self::TYPE_TOUTIAO => '头条',
            self::TYPE_XINSHIDAI => '新时代',
            self::TYPE_PANYU => '番禺',
            self::TYPE_REDIAN => '热点',
            self::TYPE_XUETANG => '微学堂',
            self::TYPE_WEISHI => '微视',
            self::TYPE_WENHUA => '文化',
            self::TYPE_HUODONG => '活动',
            self::TYPE_XINMEITI => '新媒体矩阵',
            self::TYPE_WENZHENG => '问政',
            self::TYPE_DUBAO => '读报'
        ];
    }



}