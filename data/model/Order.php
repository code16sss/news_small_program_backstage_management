<?php
namespace data\model;

class Order extends BaseModel{

    public function getNewAttr($value)
    {
        return News::where('id','=',$value)->value('title');
    }

    public function getUserAttr($value)
    {
        return User::where('id','=',$value)->value('nickname');
    }

    public function getCoverAttr($value,$data)
    {
        return User::where('id','=',$data['user'])->value('avatarurl');
    }

    public function setUserAttr($value)
    {
        return User::where('openid','=',$value)->value('id');
    }
}