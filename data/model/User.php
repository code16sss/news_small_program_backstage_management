<?php
namespace data\model;

class User extends BaseModel{

    const IDEN_ADMIN = 1;   //管理员
    const IDEN_FANS = 2;    //游客

    protected $createTime = 'create_at';

    protected function setUserAttr()
    {
        return session('user');
    }

    protected function getHomeAttr($value,$data)
    {
        return $data['province'].'-'.$data['city'];
    }

    protected function getGenderAttr($value)
    {
        $sex = [
            0=>'不详',
            1=>'男',
            2=>'女'
        ];

        return $sex[$value];
    }


    /**
     * 通过用户名获取用户信息
     * @desc
     * @param $username
     * @param string $field
     * @author 16
     * @date 2018/3/20
     */
    public function getUserByUsername($account,$field='*'){
        return $this::where(['account'=>$account])->field($field)->find();
    }
}