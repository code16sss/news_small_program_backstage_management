<?php
namespace data\model;

use think\Model;

class BaseModel extends Model{

    protected $createTime = 'create_at';

    /**
     * 获取分类列表
     * @param $page int 第几页
     * @param $size int 每页几条数据
     * @param extra array key 关键词 type 分类
     */
    public function getInfoByPage($page, $size,$field='*',$extra){
        $map = [];
        $append = [];
        isset($extra['where']) && $map = $extra['where'];
        $order ='id DESC';
        isset($extra['order']) && $order = $extra['order'];
        isset($extra['append']) && $append = $extra['append'];
        return $this->field($field)->where($map)->page($page,$size)->order($order)->select()->append($append)->toArray();
    }

    /**
     * 获取相应数据的总数
     */
    public function getInfoCount($extra){
        $map=null;
        isset($extra['where']) && $map = $extra['where'];

        return $this->where($map)->count();
    }
}